"use strict";

const randomNum = () => {
  const rand = Math.floor(Math.random() * 100);
  console.log(rand);
  return rand;
};

const askInput = (tries) =>
  Number.parseInt(
    prompt(`Escribe un numero entre 0 y 100.
Tu Tienes ${tries} ${tries === 1 ? "Intentos" : "Intentos"} restantes.`)
  );

const validateAnswer = (randomInteger, userInput, tries) => {
  if (userInput !== randomInteger) {
    if (tries > 0) {
      alert(
        `Muy ${
          userInput > randomInteger ? "Alto" : "Bajo"
        }. Prueba de Nuevo... ${tries} ${tries === 1 ? "Prueba" : "Pruebas "}`
      );
    }
    return false;
  }
  return true;
};

const randomNumber = randomNum();
let isCorrect = false;
for (let i = 5; i > 0; i--) {
  console.log(i);
  const userInput = askInput(i);
  isCorrect = validateAnswer(randomNumber, userInput, i - 1);
  if (isCorrect) {
    break;
  }
}

isCorrect
  ? alert(`Lo Consieguiste !!`)
  : alert(`Triste por ti , no lo has conseguido :(.`);
